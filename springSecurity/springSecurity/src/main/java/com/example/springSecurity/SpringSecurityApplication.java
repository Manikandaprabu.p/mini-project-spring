package com.example.springSecurity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SpringSecurityApplication {
	
	@Autowired
	private Repo repo;
	
	@PostConstruct
	public void initUsers(){
		String username;
		int id;
		String password;
		
		List<User> user = Stream.of(
			new User(id=101,username="prabu",password="234"),
			new User(id=102,username="charan",password="789"),
			new User(id=103,username="kumar",password="234")
				).collect(Collectors.toList());
		repo.saveAll(user);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}

}
