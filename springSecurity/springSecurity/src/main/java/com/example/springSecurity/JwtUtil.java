package com.example.springSecurity;

import java.sql.Date;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtil {

	  private String SECRET_KEY = "secret";
	  
	  public String extractUsername(String token) {
	        return extractClaim(token, Claims::getSubject);
	    }
	  
	  public Date extractExpiration(String token) {
	        return (Date) extractClaim(token, Claims::getExpiration);
	    }
	  
	  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
	        final Claims claims = extrectAllClaims(token);
	        return claimsResolver.apply(claims);
	    }
	
	private Claims extrectAllClaims(String token) {
		return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
	}
	
	
	private Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date(0));
	}
	
	public String generateToken(String username) {
		Map<String,Object> claims = new HashMap<>();
		return createToken(claims,username);
	}
	
	private String createToken(Map<String,Object> claims ,String subject) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis()+1000*60*60*10))
				.signWith(SignatureAlgorithm.HS512, SECRET_KEY).compact();
	}
	
	public boolean validateToken(String token ,UserDetails UserDetails) {
		final String username = extractUsername(token);
		return (username.equals(UserDetails.getUsername()) && !isTokenExpired(token));
	}
	
}
