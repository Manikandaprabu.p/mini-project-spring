package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.db2.Db2repo;
import com.example.demo.model.Namevaluepair;
import com.example.demo.model.Port;
import com.example.demo.model.Register;
import com.example.demo.model.db1.Db2dao;
import com.example.demo.portcheck.PortCheck;
import com.example.demo.service.Portservices;
import com.example.demo.service.Registerservice;




@RestController
@RequestMapping("/hello")
@CrossOrigin(origins="http://localhost:4200/")
public class PortController {
	


	@Autowired
	private PortCheck portCheck;

	@Autowired
	private Portservices portservices;
	
	@Autowired
    private Registerservice registerservice;
	
	@Autowired
	private Db2repo db2repo;
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate;
	
	@PostMapping("/add")
	public String add(@RequestBody Port port) {
	 portservices.Add(port);
	 return "success";
	}
	
	@GetMapping("/get")
	public List<Port> getAll(){
		return portservices.getdata();
	}
	
	
	
	@GetMapping("/getData")
	public List<Register> getAllData(){
		return registerservice.getAlldata();
	}
	

	@GetMapping("/getDataMongo")
	public List<Db2dao> getAllDataMongo(){
		return db2repo.findAll();
	}
	
	@PostMapping("/addData")
	public Register addData(@RequestBody Register register) {
		 return registerservice.AddData(register);
	
	}
	

	@PostMapping("/login")
     public ResponseEntity<Register>login(@RequestBody Register name){
	  Register register =registerservice.loginUser(name); 
	  
	  
	  if(register.getPassword().equals(name.getPassword()))
		  return ResponseEntity.ok(register);
		  return (ResponseEntity<Register>) ResponseEntity.internalServerError();
	 }
	
	
	
	
	@GetMapping("/sts/{port}/{ip}")
	@ResponseBody
	public Namevaluepair getBYPort(@PathVariable("port") int port ,@PathVariable("ip") String ip) {		
		String str=portCheck.isRemotePortInUse(ip,port);
		Namevaluepair namevaluepair = new Namevaluepair();
		namevaluepair.setStatus(str);
		return namevaluepair;

	}
	
	

	@PostMapping("/addMongoData")
	public Db2dao addStatus(@RequestBody Db2dao db2dao) {
		return db2repo.save(db2dao);
		 
	}
	
	@PostMapping("/postData")
    public String sendDataToKafka(@RequestBody Db2dao data) {
        String emailContent = generateEmail(data);
        kafkaTemplate.send("hey",emailContent);
        return "Successfull : ";

    }
	
	private String generateEmail(Db2dao data ) {
        return  "status:"+data.getStatus()+"\n"+"value :"+data.getResult() ;

    }


	


	
	
	
	




}

