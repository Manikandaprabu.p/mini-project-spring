package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.Portrepo;
import com.example.demo.model.Port;

@Service
public class Portservices {
   
	@Autowired
	private Portrepo portrepo;

	
	public void Add(Port data) {
		portrepo.save(data);
	}
	

	public List<Port> getdata(){
		return portrepo.findAll();
	}

	
	
}
