package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.Registerrepo;
import com.example.demo.model.Register;

@Service
public class Registerservice {
	
	@Autowired
	private Registerrepo registerrepo;

	
	public Register loginUser(Register name) {
		Register register = registerrepo.findByUsername(name.getUsername());
		System.out.println(register.getUsername());
		return register;
	}
	
	
	public List<Register> getAlldata(){
		return registerrepo.findAll();
		
	}	
	
	public Register AddData(Register register) {
		 return registerrepo.save(register);
	}

	
	

}
