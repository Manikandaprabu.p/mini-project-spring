package com.example.demo.model.db1;

import lombok.Data;

@Data
public class Db2dao {
		
			private String status;
            
			private int result;

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			public int getResult() {
				return result;
			}

			public void setResult(int result) {
				this.result = result;
			}

			public Db2dao(String status, int result) {
				super();
				this.status = status;
				this.result = result;
			}

			public Db2dao() {
				super();
			}

			@Override
			public String toString() {
				return "Db2dao [status=" + status + ", result=" + result + "]";
			}
			
			
		
			
			
			
			
}
