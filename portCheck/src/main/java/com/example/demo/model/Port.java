package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="PortCheck")
public class Port {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
    
	@Column(name="ip")
	private String ip;
	
	@Column(name="port")
	private int port;
	
	@Column(name="address")
	private String address;
	
	@Column(name="projectname")
	private String projectname;
	 
	@Column(name="status")
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Port(int id, String ip, int port, String address, String projectname, String status) {
		super();
		this.id = id;
		this.ip = ip;
		this.port = port;
		this.address = address;
		this.projectname = projectname;
		this.status = status;
	}

	public Port() {
		super();
	}

	
	
	
	
}