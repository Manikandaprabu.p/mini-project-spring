package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.common.serialization.*;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;

public class ProducerConfig {

	private static final String BOOTSTRAP_SERVERS_CONFIG = null;
	private static final String KEY_SERIALIZER_CLASS_CONFIG = null;
	private static final String VALUE_SERIALIZER_CLASS_CONFIG = null;


	@Bean
    public KafkaTemplate<String , String> kafkaTemplate(){
        return new KafkaTemplate<>(producerFactory());
    }

    
    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map <String , Object> map = new HashMap<String, Object>();
        map.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        map.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        map.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(map);
    }
}
